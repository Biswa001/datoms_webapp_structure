import isDeepEqual from 'react-fast-compare';

export default (
	optimizeWithShouldComponentUpdate,
	currentProps,
	currentState,
	nextProps,
	nextState
) => {
	let componentShouldBeUpdated = true;

	if (
		optimizeWithShouldComponentUpdate &&
		isDeepEqual(currentProps, nextProps) &&
		isDeepEqual(currentState, nextState)
	) {
		componentShouldBeUpdated = false;
	}

	return componentShouldBeUpdated;
};
