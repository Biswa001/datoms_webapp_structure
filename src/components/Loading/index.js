/* Libs */
import React from 'react';
import {Layout, Spin} from 'antd';
import { bool } from 'prop-types';

/* Own Libs */
import isComponentUpdateRequired from '../../libs/IsComponentUpdateRequired';

/* Styles */
import './style.less';

/* Configs */
import defaultConfigs from './defaultConfigs';

/**
 * Represents daily - data in a calendar where each day in the calendar is represented as a certain color as per its data value. Various colors can be specified with each color representing a certain range of values.
 *
 * When To Use:
 *
 * 1. To show daily data in a calendar heat map.
 **/
export default class Loading extends React.Component {
	// initializing defaultprops, so that it will provide default configs
	static defaultProps = {
		...defaultConfigs,
	};

	static propTypes = {
		/** Should re-rendering of the component be prevented via `shouldComponentUpdate`. The value specified during mounting the component is considered as final & can't be changed further until the component stays mounted. */
		optimizeWithShouldComponentUpdate: bool,
	};

	constructor(props) {
		super(props);
		this.state = {
			message : ["Fetching your application...","Initializing engines...","Loading your data...","Few seconds more..."],
      flag : 0
		};

		this.optimizeWithShouldComponentUpdate = props.optimizeWithShouldComponentUpdate;
	}

	componentDidMount() {
    this.v=setInterval(()=> this.update(),5000);
  }

  componentWillUnmount() {
    clearInterval(this.v);
  }  

  update() {
    this.setState({flag: this.state.flag+1 })
           
    if(this.state.flag > this.state.message.length-1) {
      this.setState({ flag: 0})
    }
  }


  abx() {
    if (this.props.show_logo)
    {
      return <div ><img src="https://prstatic.phoenixrobotix.com/imgs/datoms/iot-platform/datoms_logo.svg" /></div>;   
    }
  }

	render() {
		return <Layout className={'conts' + (this.props.is_collapsed ? ' collapsed-side' :  '')} style={{marginTop: '60px'}}>
	    <div className="load-center">
        <img class="svg" src="https://prstatic.phoenixrobotix.com/imgs/index/index-page-loader.svg" alt="" />
        <div class="msg">{this.state.message[this.state.flag]}</div>
	    </div>
		  <div class="b-logo">{this.abx()}</div>    
		</Layout>;
	}
}
