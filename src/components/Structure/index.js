/* Libs */
import React, { Suspense } from 'react';
import { bool, number } from 'prop-types';
import { Layout } from 'antd';
import Icon from '@ant-design/icons';
import NewHead from './headComponents/NewHead';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import HeadSideObjectData from './HeadSideObjectData';
import Loading from '../Loading/index';
import Error404 from './Error404';

/* Own Libs */
import isComponentUpdateRequired from '../../libs/IsComponentUpdateRequired';

/* Styles */
import './style.less';

/* Configs */
import defaultConfigs from './defaultConfigs';



/**
 * Represents daily - data in a calendar where each day in the calendar is represented as a certain color as per its data value. Various colors can be specified with each color representing a certain range of values.
 *
 * When To Use:
 *
 * 1. To show daily data in a calendar heat map.
 **/
export default class Structure extends React.Component {
	// initializing defaultprops, so that it will provide default configs
	static defaultProps = {
		...defaultConfigs,
	};

	static propTypes = {
		/** Should re-rendering of the component be prevented via `shouldComponentUpdate`. The value specified during mounting the component is considered as final & can't be changed further until the component stays mounted. */
		optimizeWithShouldComponentUpdate: bool,
	};

	constructor(props) {
		super(props);
		this.state = {
			collapsed: true,
			collapse: true,
			head_side_object_data: {}
		};

		this.baseName = '';
		if (process.env.REACT_APP_BUILD_MODE !== 'development') {
			let client_slug = window.location.pathname.split('/')[2];
			if (client_slug && typeof client_slug === 'string') {
				this.baseName = '/enterprise/' + client_slug;
			}
		}

		this.onCollapseSider = this.onCollapseSider.bind(this);
		this.onMenuClick = this.onMenuClick.bind(this);
		this.updatePageType = this.updatePageType.bind(this);
		this.optimizeWithShouldComponentUpdate = props.optimizeWithShouldComponentUpdate;
		// this.accessMenuUpdate(this.props.page_type);
		console.log('structure_props_', this.props);
	}

	shouldComponentUpdate(nextProps, nextState) {
		return isComponentUpdateRequired(
			this.optimizeWithShouldComponentUpdate,
			this.props,
			this.state,
			nextProps,
			nextState
		);
	}

	/*accessMenuUpdate(page_type = this.props.page_type) {
		console.log('accessMenuUpdate enabled_features', this.props.enabled_features);
		if (this.props.enabled_features) {
			let head_side_object_data = JSON.parse(
					JSON.stringify(HeadSideObjectData)
				),
				app_name = '';

			if (this.props.application_id !== 12) {
				if (
					this.props.enabled_features.includes(
						'Reports:Customized'
					) ||
					this.props.enabled_features.includes(
						'Reports:SaveTemplates'
					) ||
					this.props.enabled_features.includes('Reports:Scheduled')
				) {
					head_side_object_data.dg_monitoring.header_component.sider.menu_items.push(
						{
							key: 'reports',
							icon_type: 'file-text',
							page_name: this.props.t('reports'),
							name: 'Reports',
							url: '/dg-monitoring/reports/',
						}
					);
				}

				if (
					this.props.enabled_features.includes(
						'AlertManagement:Configure'
					) ||
					this.props.enabled_features.includes(
						'AlertManagement:Templates'
					)
				) {
					let submenu_items_delivery = [],
						submenu_items_dg = [];
					if (
						this.props.enabled_features.includes(
							'AlertManagement:Configure'
						)
					) {
						submenu_items_delivery.push({
							key: 'alerts-management',
							name: 'Alert Management',
							url: '/delivery-tracking/alerts-management/',
						});
					}
					if (
						this.props.enabled_features.includes(
							'AlertManagement:Templates'
						)
					) {
						submenu_items_delivery.push({
							key: 'alerts',
							name: 'Alert Template',
							url: '/delivery-tracking/alerts/',
						});
					}
					head_side_object_data.temperature_monitoring.header_component.sider.menu_items.push(
						{
							key: 'alerts',
							icon_type: 'bell',
							page_name: 'Alerts',
							submenu_items: submenu_items_delivery,
						}
					);

					if (this.props.template_id !== 17) {
						if (
							this.props.enabled_features.includes(
								'AlertManagement:Configure'
							)
						) {
							submenu_items_dg.push({
								key: 'alerts-management',
								name: 'Alert Management',
								url: '/dg-monitoring/alerts-management/',
							});
						}
						if (
							this.props.enabled_features.includes(
								'AlertManagement:Templates'
							)
						) {
							submenu_items_dg.push({
								key: 'alerts-template',
								name: 'Alert Template',
								url: '/dg-monitoring/alerts-template/',
							});
						}
						head_side_object_data.dg_monitoring.header_component.sider.menu_items.push(
							{
								key: 'Alerts',
								icon_type: 'bell',
								page_name: 'Alerts',
								url: '/dg-monitoring/',
								submenu_items: submenu_items_dg,
							}
						);
					}
				}

				if (
					this.props.enabled_features.includes(
						'UserManagement:Users'
					) ||
					this.props.enabled_features.includes(
						'UserManagement:UserManagement'
					)
				) {
					head_side_object_data.temperature_monitoring.header_component.sider.menu_items.push(
						{
							key: 'user-management',
							icon_type: 'user',
							page_name: 'User Management',
							url:
								'/delivery-tracking/user-management/users/view',
						}
					);

					head_side_object_data.dg_monitoring.header_component.sider.menu_items.push(
						{
							key: 'user-management',
							icon_type: 'user',
							page_name: 'User Management',
							url: '/dg-monitoring/user-management/users/view',
						}
					);
				}

				if (
					this.props.enabled_features.includes(
						'Notifications:Notifications'
					)
				) {
					head_side_object_data.dg_monitoring.header_component.sider.menu_items.push(
						{
							key: 'Notifications',
							icon_type: 'bell',
							page_name: this.props.t('notifications'),
							name: 'Notifications',
							url: '/dg-monitoring/notifications',
						}
					);
				}

				if (
					this.props.enabled_features.includes(
						'DeviceManagement:View'
					) ||
					this.props.enabled_features.includes(
						'DeviceManagement:DeviceManagement'
					)
				) {
					head_side_object_data.iot_platform.header_component.sider.menu_items.push(
						{
							key: 'devices',
							translate_key: 'devices',
							icon_type: 'tablet',
							page_name: 'Devices',
							url: '/iot-platform/devices/assigned',
						}
					);
				}

				if (
					this.props.enabled_features.includes(
						'ThingManagement:ThingAdd'
					) ||
					this.props.enabled_features.includes(
						'ThingManagement:ThingList'
					)
				) {
					head_side_object_data.iot_platform.header_component.sider.menu_items.push(
						{
							key: 'thing',
							icon_type: 'plus-circle',
							page_name: 'Thing Management',
							url: '/iot-platform/things',
						}
					);
				}
			}

			if (this.props.application_id == 20) {
				app_name = 'temperature_monitoring';
			} else if (this.props.application_id == 16) {
				app_name = 'dg_monitoring';
			} else if (this.props.application_id == 17) {
				app_name = 'iot_platform';
			} else if (this.props.application_id == 12) {
				app_name = 'datoms_x';
				head_side_object_data[
					app_name
				].header_component.sider.menu_items = this.setDatomsXMenuGenerate(
					page_type
				);
			}
			if (this.props.application_id != 12) {
				head_side_object_data[
					app_name
				].header_component.header.logo = this.props.client_logo;
				head_side_object_data[
					app_name
				].header_component.header.vendor_logo = this.props.vendor_logo;
				head_side_object_data[
					app_name
				].header_component.header.company_name = this.props.client_name;
			}

			this.setState(
				{
					page_type: page_type,
					head_side_object_data: head_side_object_data[app_name],
				},
				() => {
					console.log(
						'head_side_object_data',
						this.state.head_side_object_data
					);
				}
			);
		}
	}*/

	onCollapseSider() {
		console.log('old collapse -> ', this.state.collapsed);
		this.setState(
			{
				collapsed: !this.state.collapsed,
			},
			() => {
				console.log('new collapse -> ', this.state.collapsed);
			}
		);
	}

	onMenuClick() {
		this.setState({
			updated: true,
		});
	}

	updatePageType() {
		let page_type = 'device',
			page_path = '';

		if (window.location.pathname.split('/')[2]) {
			page_path = window.location.pathname.split('/')[2];
			if (process.env.REACT_APP_BUILD_MODE !== 'development') {
				page_path = window.location.pathname.split('/')[4];
			}
		}
		if (
			page_path == 'production-inventory' ||
			page_path == 'sim-management'
		) {
			page_type = 'production';
		} else if (page_path == 'client-management') {
			page_type = 'client';
		}
		// console.log('page_type_1', page_type);

		this.accessMenuUpdate(page_type);
	}

	render() {
		console.log('head_side_object_data_', this.props.head_side_object_data);
		if (Object.keys(this.props.head_side_object_data).length) {
		console.log('head_side_object_data_ true');

			return <Router basename={this.baseName}>
				<Suspense
					fallback={
						<div>
							<NewHead
								user_preferences={
									this.props.user_preferences
								}
								user_id={this.props.user_id}
								client_id={this.props.client_id}
								application_id={
									this.props.application_id
								}
								new_head={
									this.props.head_side_object_data
								}
								collapsed={this.state.collapsed}
								onCollapseSider={this.onCollapseSider}
								location_path={window.location.href}
							/>
							<Layout
								className={
									'contains' +
									(this.state.collapsed
										? ' collapsed-side'
										: '')
								}
							>
								<Loading ta={true} />
							</Layout>
						</div>
					}
				>
					<NewHead
						t={this.props.t}
						i18n={this.props.i18n}
						user_preferences={this.props.user_preferences}
						user_id={this.props.user_id}
						client_id={this.props.client_id}
						application_id={this.props.application_id}
						new_head={this.props.head_side_object_data}
						location_path={window.location.href}
						collapsed={this.state.collapsed}
						onCollapseSider={this.onCollapseSider}
						onmenuClick={this.onMenuClick}
						active_page_type={this.props.page_type}
						updatePageType={this.updatePageType}
					/>
					<Layout
						className={
							'page-contains' +
							(this.state.collapsed
								? ' collapsed-side'
								: '')
						}
					>
						<Switch>
							{this.props.children}
							<Route component={Error404} />
						</Switch>
					</Layout>
				</Suspense>
			</Router>;
		} else {
			return false;
		}	
	}
}
