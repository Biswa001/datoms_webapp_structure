import React from 'react';
import Structure from './';

export default {
	title: 'Structure',
	component: Structure,
};

export const ZeroConfig = () => {
	return (
		<Structure />
	);
};
