import React from 'react';
// import NavLink from './NavLink';
import { Link } from 'react-router-dom';
// import { ReactComponent as ErrorSmileySad } from '../../../images/DatomsX/error_smiley_sad.svg';
/**
 * Component for the error page.
 */
export default class Error404 extends React.Component {
	/**
	 * This is the Constructor for Dashboard page used to set the default task while page loads.
	 * @param  {Object} props This will import the attributes passed from its Parent Class.
	 */
	constructor(props) {
		super(props);
		console.log('this.props.location', props);
		this.path_name = '/datoms-x/';
		if (props.history && props.history.location && props.history.location.pathname && props.history.location.pathname.includes('/iron-and-steel-industry')) {
			this.path_name = '/iron-and-steel-industry';
		} else if (props.history && props.history.location && props.history.location.pathname && props.history.location.pathname.includes('/datoms-x')) {
			this.path_name = '/datoms-x';
		} else if (props.history && props.history.location && props.history.location.pathname && props.history.location.pathname.includes('/delivery-tracking')) {
			this.path_name = '/delivery-tracking';
		} else if (props.history && props.history.location && props.history.location.pathname && props.history.location.pathname.includes('/iot-platform')) {
			this.path_name = '/iot-platform';
		} else if (props.history && props.history.location && props.history.location.pathname && props.history.location.pathname.includes('/dg-monitoring')) {
			this.path_name = '/dg-monitoring';
		}
		this.state = {
			collapse: true,
			current_header_menu: this.props.location && Object.keys(this.props.location).length && this.props.location.pathname ? ((this.props.location.pathname.includes('sim-management') || this.props.location.pathname.includes('production-inventory')) ? 'production' : (this.props.location.pathname.includes('client-management') ? 'client' : '')) : ''
		}
	}
	/**
	 * Predefined function of ReactJS.
	 */
	componentDidMount() {
		document.title = 'Error404 - Datoms IOT Platform';
	}

	collapseState(collapse) {
		this.setState({
			collapse: collapse
		});
	}

	changeHeader(e) {
		this.setState({
			current_header_menu: e
		});
	}

	/**
	 * Predefined function of ReactJS to render the component.
	 * @return {Object}
	 */
	render() {
		return(
			<div>
				<div className="full-page-container">
					{/*<NavLink />*/}
					<div className="background">
						<center className="center-error">
							<div className="data-err-page">
								<div className="data-err-page-img">
									<svg xmlns="http://www.w3.org/2000/svg"><g fill="#5D5D5D"><path d="M33.067 66.136C14.834 66.136 0 51.302 0 33.068S14.834.001 33.067.001c18.234 0 33.07 14.834 33.07 33.067s-14.835 33.068-33.07 33.068zm0-62.135C17.039 4.001 4 17.04 4 33.068s13.039 29.067 29.067 29.067c16.029 0 29.07-13.039 29.07-29.067S49.096 4.001 33.067 4.001z"/><circle cx="24.927" cy="24.825" r="4.334"/><circle cx="41.21" cy="24.825" r="4.334"/><path d="M20.613 47.246l-2.404-3.197c18.005-13.532 30.864.108 30.992.247l-2.943 2.709c-.427-.458-10.6-11.067-25.645.241z"/></g></svg>
								</div>
								<div className="data-err-page-msg">{"Sorry, the page you were looking for wasn't found."}</div>
								<a href={'https://app.datoms.io'}>
									<button type="button" className="err-btn redirect-home">Go To Home Page</button>
								</a>
							</div>
						</center>
					</div>
				</div>
			</div>
		);
	}
}
