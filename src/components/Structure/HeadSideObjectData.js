import phoenix_logo_with_text from './phoenix_logo_with_text.png';
import technosoft_logo from './technosoft_logo.png';

export default {
	dg_monitoring: {
		header_component: {
			header: {
				logo: phoenix_logo_with_text,
				vendor_logo: '',
				notification_enabled: true,
				notification_drop: [],
				company_name: 'Phoenix Robotix',
				page_name: '',
				change_pass_link: 'https://accounts.datoms.io/change-password',
				logout: 'https://accounts.datoms.io/logout',
				bg_color: '#fff',
				selected_menu_color: '#fff',
			},
			sider: {
				collapsible: true,
				non_collapse_logo:
					'https://prstatic.phoenixrobotix.com/imgs/cold_storage_monitoring/datoms_logo.svg',
				collapse_logo: 'https://static.datoms.io/images/d_logo.svg',
				menu_items: [
					{
						key: 'dashboard',
						translate_key: 'dashboard',
						icon_type: 'area-chart',
						page_name: 'Dashboard',
						url: '/dg-monitoring/dashboard',
					},
					{
						key: 'dg-sets',
						icon_type: 'layout',
						page_name: 'DG Sets',
						translate_key: 'dg_sets',
						url: '/dg-monitoring/dg-sets/',
						submenu_items: [
							{
								key: 'detailed-view',
								translate_key: 'detailed_view',
								name: 'Detailed View',
								url: '/dg-monitoring/dg-sets/detailed-view/',
							},
							{
								key: 'panel-view',
								translate_key: 'panel_view',
								name: 'Panel View',
								url: '/dg-monitoring/dg-sets/panel-view',
							},
							{
								key: 'list-view',
								translate_key: 'list_view',
								name: 'List View',
								url: '/dg-monitoring/dg-sets/list-view',
							},
						],
					},
				],
				bg_color: '#001529',
				selected_menu_color: '#fff',
			},
		},
	},

	datoms_x: {
		header_component: {
			header: {
				logo: phoenix_logo_with_text,
				vendor_logo: '',
				company_name: 'Phoenix Robotix',
				page_name: '',
				change_pass_link: 'https://accounts.datoms.io/change-password',
				logout: 'https://accounts.datoms.io/logout',
				bg_color: '#fff',
				user_name: '',
				selected_menu_color: '#fff',
				menu_items: [
					{
						key: 'device',
						icon_type: 'switcher',
						page_name: 'DATOMS',
						url: '/datoms-x/dashboard',
					},
					{
						key: 'production',
						icon_type: 'tool',
						page_name: 'Production',
						url: '/datoms-x/production-inventory/circuits/',
					},
					{
						key: 'client',
						icon_type: 'user',
						page_name: 'Client',
						url: '/datoms-x/client-management/',
					},
				],
			},
			sider: {
				collapsible: true,
				non_collapse_logo:
					'https://prstatic.phoenixrobotix.com/imgs/cold_storage_monitoring/datoms_logo.svg',
				collapse_logo: 'https://static.datoms.io/images/d_logo.svg',
				menu_items: [],
				bg_color: '#001529',
				selected_menu_color: '#fff',
			},
		},
	},

	iot_platform: {
		header_component: {
			header: {
				logo: phoenix_logo_with_text,
				vendor_logo: '',
				company_name: 'Phoenix Robotix',
				page_name: '',
				change_pass_link: 'https://accounts.datoms.io/change-password',
				logout: 'https://accounts.datoms.io/logout',
				bg_color: '#fff',
				user_name: '',
				selected_menu_color: '#fff',
			},
			sider: {
				collapsible: true,
				non_collapse_logo:
					'https://prstatic.phoenixrobotix.com/imgs/cold_storage_monitoring/datoms_logo.svg',
				collapse_logo: 'https://static.datoms.io/images/d_logo.svg',
				menu_items: [
					{
						key: 'customer-management',
						translate_key: 'customer_management',
						icon_type: 'user',
						page_name: 'Customers',
						url: '/iot-platform/customer-management/',
					},
					{
						key: 'users',
						translate_key: 'users',
						icon_type: 'smile',
						page_name: 'Users',
						url: '/iot-platform/user-management/users/view',
					},
					/*{
						key: 'settings',
						icon_type: 'setting',
						page_name: 'Settings',
						translate_key: 'settings',
						url: '/iot-platform/settings/',
						submenu_items: [
							{
								key: 'users',
								name: 'Users',
								translate_key: 'users',
								url:
									'/iot-platform/settings/user-management/users/view',
							},
						],
					},*/
				],
				bg_color: '#001529',
				selected_menu_color: '#fff',
			},
		},
	},

	temperature_monitoring: {
		header_component: {
			header: {
				logo: technosoft_logo,
				vendor_logo: '',
				company_name: 'Phoenix Robotix',
				page_name: '',
				change_pass_link: 'https://accounts.datoms.io/change-password',
				logout: 'https://accounts.datoms.io/logout',
				preference: true,
				bg_color: '#fff',
				selected_menu_color: '#fff',
			},
			sider: {
				collapsible: true,
				non_collapse_logo:
					'https://prstatic.phoenixrobotix.com/imgs/cold_storage_monitoring/datoms_logo.svg',
				collapse_logo: 'https://static.datoms.io/images/d_logo.svg',
				menu_items: [
					{
						key: 'dashboard',
						translate_key: 'dashboard',
						icon_type: 'layout',
						page_name: 'Dashboard',
						url: '/delivery-tracking/dashboard',
					},
					{
						key: 'loggers',
						translate_key: 'loggers',
						icon_type: 'appstore',
						page_name: 'Loggers',
						url: '/delivery-tracking/loggers',
					},
				],
				bg_color: '#001529',
				selected_menu_color: '#fff',
			},
		},
	},

	energy_monitoring: {
		header_component: {
			header: {
				logo: phoenix_logo_with_text,
				vendor_logo: '',
				company_name: 'Phoenix Robotix',
				page_name: '',
				change_pass_link: 'https://accounts.datoms.io/change-password',
				logout: 'https://accounts.datoms.io/logout',
				bg_color: '#fff',
				selected_menu_color: '#fff',
			},
			sider: {
				collapsible: true,
				non_collapse_logo:
					'https://prstatic.phoenixrobotix.com/imgs/cold_storage_monitoring/datoms_logo.svg',
				collapse_logo: 'https://static.datoms.io/images/d_logo.svg',
				menu_items: [
					{
						key: '1',
						icon_type: 'layout',
						page_name: '',
						submenu_items: [
							{
								key: 'energy-panel',
								name: 'Panel View',
								url: '/energy-monitoring/energy-panel',
							},
							{
								key: 'list-view',
								name: 'List View',
								url: '/energy-monitoring/list-view',
							},
						],
					},
				],

				bg_color: '#001529',
				selected_menu_color: '#fff',
			},
		},
	},

	pollution_monitoring: {
		header_component: {
			header: {
				logo: phoenix_logo_with_text,
				vendor_logo: '',
				company_name: 'Phoenix Robotix',
				change_pass_link: 'https://accounts.datoms.io/change-password',
				logout: 'https://accounts.datoms.io/logout',
				bg_color: '#fff',
				selected_menu_color: '#fff',
			},
			sider: {
				collapsible: true,
				non_collapse_logo:
					'https://prstatic.phoenixrobotix.com/imgs/cold_storage_monitoring/datoms_logo.svg',
				collapse_logo: 'https://static.datoms.io/images/d_logo.svg',
				menu_items: [
					{
						key: 'dashboard',
						icon_type: 'layout',
						page_name: 'Dashboard',
						submenu_items: [
							{
								key: 'panel-view',
								name: 'Panel View',
								url: '/pollution-monitoring/panel-view',
							},
							{
								key: 'detailed-view',
								name: 'Detailed View',
								url: '/pollution-monitoring/detailed-view',
							},
							{
								key: 'list-view',
								name: 'List View',
								url: '/pollution-monitoring/list-view',
							},
							{
								key: 'asish-test',
								name: 'Asish Test',
								url:
									'/pollution-monitoring/dashboard/asish-test',
							},
						],
					},
				],
				bg_color: '#001529',
				selected_menu_color: '#fff',
			},
		},
	},

	gas_analysis_system: {
		header_component: {
			header: {
				logo: phoenix_logo_with_text,
				vendor_logo: '',
				company_name: 'Phoenix Robotix',
				page_name: '',
				change_pass_link: 'https://accounts.datoms.io/change-password',
				logout: 'https://accounts.datoms.io/logout',
				preference: true,
				bg_color: '#fff',
				selected_menu_color: '#fff',
			},
			sider: {
				collapsible: true,
				non_collapse_logo:
					'https://prstatic.phoenixrobotix.com/imgs/cold_storage_monitoring/datoms_logo.svg',
				collapse_logo: 'https://static.datoms.io/images/d_logo.svg',
				menu_items: [
					{
						key: 'nh3-analysis',
						icon_type: 'line-chart',
						page_name: 'NH3 Analysis',
						url: '/gas-analysis-system',
					},
					{
						key: 'remote-varification',
						icon_type: 'dot-chart',
						page_name: 'Verification Test',
						url:
							'/gas-analysis-system/remote-varification/linearity-check-test',
					},
					{
						key: 'reports',
						icon_type: 'file-text',
						page_name: 'Reports',
						url: '/gas-analysis-system/reports',
					},
					{
						key: 'settings',
						icon_type: 'setting',
						page_name: 'Settings',
						url: '/gas-analysis-system/settings/nh3-test',
					},
				],
				bg_color: '#001529',
				selected_menu_color: '#fff',
			},
		},
	},

	aurassure: {
		header_component: {
			header: {
				logo: phoenix_logo_with_text,
				vendor_logo: '',
				company_name: 'Phoenix Robotix',
				page_name: '',
				change_pass_link: 'https://accounts.datoms.io/change-password',
				logout: 'https://accounts.datoms.io/logout',
				preference: true,
				bg_color: '#fff',
				selected_menu_color: '#fff',
			},
			sider: {
				collapsible: true,
				non_collapse_logo:
					'https://prstatic.phoenixrobotix.com/imgs/cold_storage_monitoring/datoms_logo.svg',
				collapse_logo: 'https://static.datoms.io/images/d_logo.svg',
				menu_items: [
					{
						key: 'dashboard',
						icon_type: 'line-chart',
						page_name: 'Dashboard',
						url: '/aurassure/dashboard',
					},
				],
				bg_color: '#001529',
				selected_menu_color: '#fff',
			},
		},
	},
};
