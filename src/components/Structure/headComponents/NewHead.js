import React from  'react';
import Head from './Head';
import Sider from './Sider';
import _ from 'lodash';
import {Layout} from 'antd';
import './menu.less';

export default class NewHead extends React.Component {
    constructor(props) {
        super(props);
        console.log('new_head_props', this.props);
        let menuOptions = this.props.new_head && this.props.new_head.header_component ? this.props.new_head.header_component.sider.menu_items : [];

        this.state = {
            active_menu_key: '',
            active_submenu_key: '',
            menuOptions: menuOptions,
            SiderOptions: this.props.new_head.header_component.sider
        }
        this.onCollapse = this.onCollapse.bind(this);
        this.currentMenuData = this.currentMenuData.bind(this);
        this.currentSubMenuData = this.currentSubMenuData.bind(this);
    }

    onCollapse() {
        // console.log('collapsing');
        this.props.onCollapseSider();
        setTimeout(() => {
            window.dispatchEvent(new Event('resize'));
        }, 500);
    };

    componentDidUpdate(prevProps, prevState) {
        if (this.props.active_page_type !== prevProps.active_page_type && this.props.location_path.includes('/datoms-x')) {
            let menuOptions = this.props.new_head && this.props.new_head.header_component ? this.props.new_head.header_component.sider.menu_items : [];
        console.log('componentDidUpdate_', menuOptions);
            this.setState({
                menuOptions: menuOptions
            }, () => {
                let currentPageDetails = _.find(this.props.new_head.header_component.sider.menu_items, (o) => {
                    return this.props.location_path.includes(o.url);
                });

                if(currentPageDetails) {
                    this.currentMenuData(currentPageDetails);
                }
            });
        }
    }

    currentMenuData(activeMenuData) {
        let that = this;
        console.log('currentMenuData value', activeMenuData);
        if (activeMenuData) {
            // console.log('currentMenuData value_1', this.state.menuOptions);
            let activeSubMenuData = '';
            if (activeMenuData.submenu_items && activeMenuData.submenu_items.length) {
                activeMenuData.submenu_items.map((submenuData, index) => {
                    if (window.location.pathname.includes(submenuData.url)) {
                        activeSubMenuData = submenuData;
                    }
                });
            }
            console.log('activeSubMenuData_', activeSubMenuData);
            that.setState({
                active_menu_key: activeMenuData.key,
                active_submenu_key: activeSubMenuData ? activeSubMenuData.key : ''
            }, () => {
                that.props.onmenuClick();
            });
        }
    }

    currentSubMenuData(activeSubMenuData) {
        let that = this;
        // console.log('currentSubMenuData value', activeSubMenuData);
        that.setState({
            active_menu_key: '',
            active_submenu_key: activeSubMenuData ? activeSubMenuData.key : ''
        }, () => {
            that.props.onmenuClick();
        });
    }

    componentDidMount() {
        let application_page_name = '';
        if (process.env.REACT_APP_BUILD_MODE !== 'development') {
            application_page_name = window.location.pathname.split('/')[4];
        } else {
            application_page_name = window.location.pathname.split('/')[2];
        }
        let currentPageDetails = _.find(this.props.new_head.header_component.sider.menu_items, (o) => {
            return this.props.location_path.includes(o.url);
        });

        if(currentPageDetails) {
            this.currentMenuData(currentPageDetails);
        }
    }
    
    render() {
        const {t,i18n} = this.props;
      return <Layout className="menu-component">
        <Head t={t} i18n={i18n} user_preferences={this.props.user_preferences} client_id={this.props.client_id} user_id={this.props.user_id} application_id={this.props.application_id} SiderOptions={this.props.new_head && this.props.new_head.header_component ? this.props.new_head.header_component.sider : ''} HeaderOptions={this.props.new_head && this.props.new_head.header_component ? this.props.new_head.header_component.header : ''} location_path={this.props.location_path} collapsed={this.props.collapsed} currentMenuData={(menu_value) => this.currentMenuData(menu_value)} active_menu_key={this.state.active_menu_key} currentSubMenuData={this.currentSubMenuData} active_page_type={this.props.active_page_type ? this.props.active_page_type : ''} onCollapse={this.onCollapse} active_submenu_key={this.state.active_submenu_key} menuOptions={this.state.menuOptions} updatePageType={() => this.props.updatePageType()}/>

        <Sider t={t} collapsed={this.props.collapsed} onCollapseSider={this.props.onCollapseSider} SiderOptions={this.props.new_head && this.props.new_head.header_component ? this.props.new_head.header_component.sider : []} location_path={this.props.location_path} onCollapse={this.onCollapse} currentMenuData={(menu_value) => this.currentMenuData(menu_value)} currentSubMenuData={this.currentSubMenuData} active_menu_key={this.state.active_menu_key} active_submenu_key={this.state.active_submenu_key} menuOptions={this.state.menuOptions}/>
      </Layout>
    }
}