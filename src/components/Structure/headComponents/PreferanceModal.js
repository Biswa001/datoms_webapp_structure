import React from 'react';
import { Link } from 'react-router-dom';
import { InfoCircleOutlined } from '@ant-design/icons';
import {
	Layout,
	Menu,
	Divider,
	Drawer,
	Select,
	Modal,
	notification,
	Button,
} from 'antd';
import Sider from './Sider';
// import CustomColorPicker from '../CustomColorPicker';
import moment from 'moment-timezone';
import { Helmet } from 'react-helmet';
import { setPreferences } from './AccessAPI';
import _ from 'lodash';

const { Header } = Layout;
const SubMenu = Menu.SubMenu;
const { Option } = Select;
const timeZones = moment.tz.names();
const preset_color = [
	'#666633',
	'#333300',
	'#336600',
	'#004d00',
	'#5DA027',
	'#339966',
	'#669999',
	'#006666',
];
export default class Head extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			timezone_pref:
				props.user_preferences && props.user_preferences.timezone
					? props.user_preferences.timezone
					: moment.tz.guess(),
			unit_pref:
				props.user_preferences && props.user_preferences.unit
					? props.user_preferences.unit
					: 'K',
			language_pref:
				props.user_preferences && props.user_preferences.language
					? props.user_preferences.language
					: 'en',
			/*parameter_color: {
				temperature: props.user_preferences && props.user_preferences.parameter_color && props.user_preferences.parameter_color.temperature ? props.user_preferences.parameter_color.temperature : '',
				humidity: props.user_preferences && props.user_preferences.parameter_color && props.user_preferences.parameter_color.humidity ? props.user_preferences.parameter_color.humidity : '',
				pressure: props.user_preferences && props.user_preferences.parameter_color && props.user_preferences.parameter_color.pressure ? props.user_preferences.parameter_color.pressure : '',
				note: props.user_preferences && props.user_preferences.parameter_color && props.user_preferences.parameter_color.note ? props.user_preferences.parameter_color.note : '',
				violation: props.user_preferences && props.user_preferences.parameter_color && props.user_preferences.parameter_color.violation ? props.user_preferences.parameter_color.violation : ''
			},
			parameter_color_view: {
				temperature: props.user_preferences && props.user_preferences.parameter_color && props.user_preferences.parameter_color.temperature ? props.user_preferences.parameter_color.temperature : '',
				humidity: props.user_preferences && props.user_preferences.parameter_color && props.user_preferences.parameter_color.humidity ? props.user_preferences.parameter_color.humidity : '',
				pressure: props.user_preferences && props.user_preferences.parameter_color && props.user_preferences.parameter_color.pressure ? props.user_preferences.parameter_color.pressure : '',
				note: props.user_preferences && props.user_preferences.parameter_color && props.user_preferences.parameter_color.note ? props.user_preferences.parameter_color.note : '',
				violation: props.user_preferences && props.user_preferences.parameter_color && props.user_preferences.parameter_color.violation ? props.user_preferences.parameter_color.violation : ''
			},*/
			// show_reset: false
		};

		// console.log('preferences_props_', this.props);
	}

	async setUserPreferences() {
		let preferences = {
			timezone: this.state.timezone_pref,
			unit: this.state.unit_pref,
			// parameter_color: this.state.parameter_color,
			// language: document.querySelector(".goog-te-combo").value
		};
		console.log('response_1', preferences);
		let response = await setPreferences(
			this.props.user_id,
			this.props.client_id,
			this.props.application_id,
			preferences
		);
		if (response.status === 'success') {
			this.openNotification('success', 'Preferences changed');
			// document.cookie = 'googtrans=/auto/' + document.querySelector(".goog-te-combo").value;
			// Reload the app
			window.location.reload();
		} else {
			this.setState({
				butn_loading: false,
			});
			this.openNotification('error', response.message);
		}
	}

	openNotification(type, msg) {
		notification[type]({
			message: msg,
			// description: msg,
			placement: 'bottomLeft',
			className: 'alert-' + type,
		});
	}

	changePreferenceSelect(value, type) {
		let timezone = this.state.timezone_pref,
			unit = this.state.unit_pref,
			language = this.state.language_pref;
		if (type === 'timezone') {
			timezone = value;
		} else if (type === 'unit') {
			unit = value;
		} else if (type === 'language') {
			language = value;
		}

		this.setState({
			timezone_pref: timezone,
			unit_pref: unit,
			language_pref: language,
		});
	}

	closeModal() {
		this.setState(
			{
				timezone_pref:
					this.props.user_preferences &&
					this.props.user_preferences.timezone
						? this.props.user_preferences.timezone
						: this.state.timezone_pref,
				unit_pref:
					this.props.user_preferences &&
					this.props.user_preferences.unit
						? this.props.user_preferences.unit
						: this.state.unit_pref,
				language_pref:
					this.props.user_preferences &&
					this.props.user_preferences.language
						? this.props.user_preferences.language
						: this.state.language_pref,
				/*parameter_color: {
				temperature: '',
				humidity: '',
				pressure: '',
				note: '',
				violation: ''
			}*/
			},
			() => {
				document.cookie =
					'googtrans=/auto/' +
					(this.props.user_preferences &&
					this.props.user_preferences.language
						? this.props.user_preferences.language
						: 'en');
				this.props.closePreferenceModal();
			}
		);
	}

	changePreference() {
		console.log('changePreference_');
		this.setUserPreferences();
		this.setState(
			{
				changeLng: this.state.changeLng,
			},
			() => {
				this.changeLanguage(this.state.changeLng);
				this.changeLanguage(this.state.changeLng);
			}
		);
	}

	changeLanguage(lng) {
		console.log('lng123', lng);
		const { t, i18n } = this.props;
		i18n.changeLanguage(lng);
	}

	onChangeLanguage(lng) {
		console.log('changeLng1', lng);
		this.setState({
			changeLng: lng,
		});
	}

	/*handleColorChange(color, paramKey){
		this.setState((state, props) => {
			let stateToUpdate = {
				parameter_color: state.parameter_color,
				show_reset: true
			};

			if (paramKey === 'temperature') {
				stateToUpdate.parameter_color.temperature = color;
			} else if (paramKey === 'humidity') {
				stateToUpdate.parameter_color.humidity = color;
			} else if (paramKey === 'pressure') {
				stateToUpdate.parameter_color.pressure = color;
			} else if (paramKey === 'note') {
				stateToUpdate.parameter_color.note = color;
			} else if (paramKey === 'violation') {
				stateToUpdate.parameter_color.violation = color;
			}

			return stateToUpdate;
		});
  }*/

	/*colorReset() {
  	this.setState({
  		parameter_color: {
				temperature: '',
				humidity: '',
				pressure: '',
				note: '',
				violation: ''
			},
			show_reset: false
  	});
  }*/

	render() {
		// console.log('modal_propsss', this.props);
		// console.log('modal_statess', this.state);
		/*let resetButton = <div/>;
		if (this.state.show_reset || this.state.parameter_color.temperature !== '' || this.state.parameter_color.humidity !== '' || this.state.parameter_color.pressure !== '' || this.state.parameter_color.note !== '' || this.state.parameter_color.violation !== '') {
			resetButton = <div className="reset-butn"><Button type="primary" size={'small'} onClick={() => this.colorReset()}>{this.props.t ? this.props.t('reset_color') : 'Reset Colors'}</Button></div>;
		}*/
		return (
			<Modal
				className="preference-modal"
				title={this.props.t ? this.props.t('change_preferences') : ''}
				visible={this.props.visible}
				onOk={() => this.changePreference()}
				onCancel={() => this.closeModal()}
				destroyOnClose={true}
			>
				{/* <div id="google_translate_element_new" /> */}
				{/*<div className="select-container">
                    <div className="label">{this.props.t ? this.props.t('timezone') : ''} :</div>
                    <div>
                        <Select placeholder='Please select timezone' showSearch value={this.state.timezone_pref} onChange={(e) => this.changePreferenceSelect(e, 'timezone')}>
                            {(() => {
                                if (timeZones && timeZones.length) {
                                    return timeZones.map((timeZone, index) => {
                                        return <Option key={index} value={timeZone}>{timeZone}</Option>;
                                    });
                                }
                            })()}
                        </Select>
                    </div>
                </div>*/}
				<div className="select-container">
					<div className="label">
						{this.props.t
							? this.props.t('temp_unit')
							: 'Temperature Unit'}{' '}
						:
					</div>
					<div>
						<Select
							placeholder="Please select unit"
							value={this.state.unit_pref}
							onChange={(e) =>
								this.changePreferenceSelect(e, 'unit')
							}
						>
							<Option value="°C">Degree Celsius (°C)</Option>
							<Option value="°F">Degree Fahrenheit (°F)</Option>
							<Option value="K">Kelvin (K)</Option>
						</Select>
					</div>
				</div>
				{/*<div className="information-meaasge"><Icon type="info-circle" /><span className="info-msg">After changing this configuration the application will reload.</span></div>*/}
				<div className="select-container">
					<div className="label">
						{this.props.t ? this.props.t('language') : 'Language'} :
					</div>
					<Select
						defaultValue={
							this.props.t &&
							this.props.t('language') === 'Language'
								? 'en'
								: 'it'
						}
						style={{ width: 120 }}
						onChange={(e) => this.onChangeLanguage(e)}
					>
						<Option value="en">English</Option>
						<Option value="it">Italian</Option>
					</Select>
				</div>
				{/*<div className="color-header">
                    <div className="color-head">{this.props.t ? this.props.t('color_pref_head') : 'Parameter Color in Graph :'}</div>
                    {resetButton}
                </div>
                <div className="select-container">
                    <div className="label">{this.props.t ? this.props.t('temperature') : 'Temperature'} :</div>
                    <CustomColorPicker
                        selectedColor={ this.state.parameter_color.temperature }
                        presetColors={preset_color}
                        defaultColor={'#5DA027'}
                onChangeColor={(color) => this.handleColorChange(color, 'temperature')}
              />
                </div>
                <div className="select-container">
                    <div className="label">{this.props.t ? this.props.t('humidity') : 'Humidity'} :</div>
                    <CustomColorPicker
                        selectedColor={ this.state.parameter_color.humidity }
                        presetColors={preset_color}
                        defaultColor={'#006666'}
                onChangeColor={(color) => this.handleColorChange(color, 'humidity')}
              />
                </div>
                <div className="select-container">
                    <div className="label">{this.props.t ? this.props.t('pressure') : 'Pressure'} :</div>
                    <CustomColorPicker
                        selectedColor={ this.state.parameter_color.pressure }
                        presetColors={preset_color}
                        defaultColor={'#666633'}
                onChangeColor={(color) => this.handleColorChange(color, 'pressure')}
              />
                </div>
                <div className="select-container">
                    <div className="label">{this.props.t ? this.props.t('note') : 'Note'} :</div>
                    <CustomColorPicker
                        selectedColor={ this.state.parameter_color.note }
                        presetColors={preset_color}
                        defaultColor={'#336600'}
                onChangeColor={(color) => this.handleColorChange(color, 'note')}
              />
                </div>
                <div className="select-container">
                    <div className="label">{this.props.t ? this.props.t('violation') : 'Violation'} :</div>
                    <CustomColorPicker
                        selectedColor={ this.state.parameter_color.violation }
                        presetColors={preset_color}
                        defaultColor={'#339966'}
                onChangeColor={(color) => this.handleColorChange(color, 'violation')}
              />
                </div>*/}
				<div className="information-meaasge">
					<InfoCircleOutlined />
					<span className="info-msg">
						{this.props.t
							? this.props.t('after_preference_modal_change')
							: ''}
					</span>
				</div>
			</Modal>
		);
	}
}
