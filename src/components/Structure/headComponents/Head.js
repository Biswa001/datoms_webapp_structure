import React from 'react';
import { Link } from 'react-router-dom';
import { LogoutOutlined, UnorderedListOutlined } from '@ant-design/icons';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import {
	Layout,
	Menu,
	Divider,
	Drawer,
	Select,
	Modal,
	notification,
} from 'antd';
import Sider from './Sider';
import PreferanceModal from './PreferanceModal';
import moment from 'moment-timezone';
import { Helmet } from 'react-helmet';

import _ from 'lodash';

const { Header } = Layout;
const SubMenu = Menu.SubMenu;
const { Option } = Select;
const timeZones = moment.tz.names();

export default class Head extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			collapsed: this.props.collapsed,
			visible_preference_modal: false,
			visible: false,
		};
	}

	showDrawer() {
		this.setState({
			visible: true,
		});
	}

	closeDrawer() {
		this.setState({
			visible: false,
		});
	}

	openPreferenceModal() {
		this.setState({
			visible_preference_modal: true,
		});
	}

	closePreferenceModal() {
		this.setState({
			visible_preference_modal: false,
		});
	}

	menuClicked(menu_value) {
		this.closeDrawer();
		this.props.currentMenuData(menu_value);
	}

	subMenuClicked() {
		this.closeDrawer();
		this.props.currentSubMenuData();
	}

	render() {
		console.log('propsss', this.props);
		let user_name = document.getElementById('user_name')
			? document.getElementById('user_name').value
			: 'Username';
		let pageName = '';
		let currentPageDetails = _.find(
			this.props.SiderOptions.menu_items,
			(o) => {
				console.log('currentPageDetails_1', this.props.location_path);
				console.log('currentPageDetails_2', o.url);
				return this.props.location_path.includes(o.url);
			}
		);
		console.log('currentPageDetails_3', currentPageDetails);

		if (currentPageDetails) {
			if (currentPageDetails.submenu_items) {
				let name = '';
				currentPageDetails.submenu_items.map((sub_menu) => {
					if (this.props.location_path.includes(sub_menu.url)) {
						name = sub_menu.name;
						return;
					}
				});
				if (name !== '') {
					pageName = name;
				}
			} else {
				pageName = currentPageDetails.page_name;
			}
		}

		let preferanceModal = <div />;
		if (this.state.visible_preference_modal) {
			preferanceModal = (
				<PreferanceModal
					visible={this.state.visible_preference_modal}
					user_preferences={this.props.user_preferences}
					closePreferenceModal={() => this.closePreferenceModal()}
					SiderOptions={this.props.SiderOptions}
					location_path={this.props.location_path}
					t={this.props.t}
					i18n={this.props.i18n}
					client_id={this.props.client_id}
					user_id={this.props.user_id}
					application_id={this.props.application_id}
				/>
			);
		}
		return (
			<Layout>
				{(() => {
					if (
						/* !document.getElementById('helmet') &&  */ document.getElementById(
							'google_translate_element'
						) &&
						!document
							.getElementById('google_translate_element')
							.contains(document.querySelector('.goog-te-gadget'))
					) {
						return (
							<span id="helmet" className="abc">
								<Helmet>
									<script
										style={{}}
										type="text/javascript"
										src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"
									></script>
								</Helmet>
							</span>
						);
					}
				})()}
				<Header
					style={{
						position: 'fixed',
						zIndex: 10,
						width: '100%',
						overflow: 'hidden',
					}}
				>
					<div>
						<div
							className={
								'logos mobile-show-content' +
								(this.props.collapsed ? ' collapsed-side' : '')
							}
						>
							<UnorderedListOutlined
								className="menu-icon"
								onClick={() => this.showDrawer()}
							/>
							<Drawer
								title=""
								placement="left"
								closable={true}
								onClose={() => this.closeDrawer()}
								visible={this.state.visible}
								className="mobile-drawer"
								destroyOnClose={true}
							>
								<Sider
									t={this.props.t}
									collapsed={this.props.collapsed}
									onCollapseSider={this.props.onCollapseSider}
									SiderOptions={this.props.SiderOptions}
									location_path={this.props.location_path}
									onCollapse={this.props.onCollapse}
									currentMenuData={(menu_value) =>
										this.menuClicked(menu_value)
									}
									currentSubMenuData={(sub_menu_value) =>
										this.subMenuClicked(sub_menu_value)
									}
									active_menu_key={this.props.active_menu_key}
									active_submenu_key={
										this.props.active_submenu_key
									}
									menuOptions={this.props.menuOptions}
								/>
							</Drawer>
						</div>
						<div
							className={
								'logos mobile-hidden-content' +
								(this.props.collapsed ? ' collapsed-side' : '')
							}
						>
							{(() => {
								if (
									this.props.HeaderOptions.vendor_logo != ''
								) {
									return (
										<img
											src={
												this.props.HeaderOptions
													.vendor_logo
											}
											className="company-logo"
										/>
									);
								}
							})()}
							{(() => {
								if (
									this.props.HeaderOptions.vendor_logo != ''
								) {
									return <Divider type="vertical" />;
								}
							})()}
							{(() => {
								if (this.props.HeaderOptions.logo != '') {
									return (
										<img
											src={this.props.HeaderOptions.logo}
											className="company-logo"
										/>
									);
								}
							})()}
							<span className="bold company-name">
								{this.props.HeaderOptions.company_name}
							</span>
							<Divider type="vertical" />
							<span>{pageName}</span>
						</div>
						<Menu
							// onClick={(e) => this.handleClick('top', e)}
							className="mobile-show-content"
							theme="light"
							mode="horizontal"
							style={{ lineHeight: '64px' }}
						>
							<SubMenu
								className="title-remove"
								title={
									<Link to="#">
										<span className="user-icon notranslate">
											{user_name.charAt(0).toUpperCase()}
										</span>
									</Link>
								}
							>
								{(() => {
									if (this.props.HeaderOptions.preference) {
										return (
											<Menu.Item key="6">
												<p
													className={
														document.querySelector(
															'.goog-te-combo'
														) &&
														document.querySelector(
															'.goog-te-combo'
														).value &&
														document.querySelector(
															'.goog-te-combo'
														).value == 'en'
															? 'notranslate'
															: ''
													}
													onClick={() =>
														this.openPreferenceModal()
													}
												>
													Preferences
												</p>
											</Menu.Item>
										);
									}
								})()}
								<Menu.Item key="4">
									<a
										href={
											this.props.HeaderOptions
												.change_pass_link
										}
										target="_blank"
									>
										Change Password
									</a>
								</Menu.Item>
								<Menu.Item key="5">
									<a href={this.props.HeaderOptions.logout}>
										<LogoutOutlined />
										Logout
									</a>
								</Menu.Item>
							</SubMenu>
						</Menu>
						<Menu
							// onClick={(e) => this.handleClick('top', e)}
							className="mobile-hidden-content"
							theme="light"
							mode="horizontal"
							style={{ lineHeight: '64px' }}
						>
							{(() => {
								if (
									this.props.HeaderOptions.menu_items &&
									this.props.HeaderOptions.menu_items.length
								) {
									return this.props.HeaderOptions.menu_items.map(
										(menu) => {
											return (
												<Menu.Item
													onClick={() =>
														this.props.updatePageType()
													}
													className={
														this.props
															.active_page_type ==
														menu.key
															? 'ant-menu-item-selected'
															: ''
													}
													key={menu.key}
												>
													<Link to={menu.url}>
														<LegacyIcon
															type={
																menu.icon_type
															}
														/>
														{menu.page_name}
													</Link>
												</Menu.Item>
											);
										}
									);
								}
							})()}
							{(() => {
								if (
									this.props.HeaderOptions.change_pass_link &&
									this.props.HeaderOptions.logout
								) {
									return (
										<SubMenu
											className="title-remove"
											title={
												<Link to="#">
													<span className="user-icon notranslate">
														{user_name
															.charAt(0)
															.toUpperCase()}
													</span>
													<span>{user_name}</span>
												</Link>
											}
										>
											{(() => {
												if (
													this.props.HeaderOptions
														.preference
												) {
													return (
														<Menu.Item key="6">
															<p
																className={
																	document.querySelector(
																		'.goog-te-combo'
																	) &&
																	document.querySelector(
																		'.goog-te-combo'
																	).value &&
																	document.querySelector(
																		'.goog-te-combo'
																	).value ==
																		'en'
																		? 'notranslate'
																		: ''
																}
																onClick={() =>
																	this.openPreferenceModal()
																}
															>
																{this.props.t
																	? this.props.t(
																			'preferences'
																	  )
																	: ''}
															</p>
														</Menu.Item>
													);
												}
											})()}
											<Menu.Item key="4">
												<a
													href={
														this.props.HeaderOptions
															.change_pass_link
													}
													target="_blank"
												>
													{this.props.t
														? this.props.t(
																'change_password'
														  )
														: 'Change Password'}
												</a>
											</Menu.Item>
											<Menu.Item key="5">
												<a
													href={
														this.props.HeaderOptions
															.logout
													}
												>
													<LogoutOutlined />
													{this.props.t
														? this.props.t('logout')
														: 'Logout'}
												</a>
											</Menu.Item>
										</SubMenu>
									);
								}
							})()}
						</Menu>
					</div>
				</Header>
				<div
					style={{ display: 'none' }}
					id="google_translate_element"
				/>
				{preferanceModal}
			</Layout>
		);
	}
}
