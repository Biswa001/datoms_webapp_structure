import React from 'react';
import { Link } from 'react-router-dom';
import { Icon as LegacyIcon } from '@ant-design/compatible';
import { Layout, Menu, Divider } from 'antd';
const { Sider } = Layout;
const SubMenu = Menu.SubMenu;

export default class NewHead extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		console.log('application_page_name', this.props);
		return (
			<Layout>
				<Sider
					collapsible={
						this.props.SiderOptions.collapsible ? true : false
					}
					className="side-menu"
					collapsed={
						window.innerWidth <= 1023 ? false : this.props.collapsed
					}
					onCollapse={() => this.props.onCollapse()}
				>
					<div className="logo" />
					{(() => {
						if (
							this.props.SiderOptions.non_collapse_logo ||
							this.props.SiderOptions.collapse_logo
						) {
							return (
								<div
									className={
										'imgs imgs-logo-container' +
										(this.props.collapsed
											? ' collapsed'
											: '')
									}
								>
									{(() => {
										if (
											this.props.SiderOptions
												.non_collapse_logo
										) {
											return (
												<img
													className={
														'big-logo' +
														(!this.props.collapsed
															? ' show'
															: ' hide')
													}
													src={
														this.props.SiderOptions
															.non_collapse_logo
													}
												/>
											);
										}
									})()}
									{(() => {
										if (window.innerWidth <= 1023) {
											return (
												<img
													className={
														'big-logo' +
														(this.props.collapsed
															? ' show'
															: ' hide')
													}
													src={
														this.props.SiderOptions
															.non_collapse_logo
													}
												/>
											);
										}
										if (
											this.props.SiderOptions
												.collapse_logo
										) {
											return (
												<img
													className={
														'small-logo' +
														(!this.props.collapsed
															? ' hide'
															: ' show')
													}
													src={
														this.props.SiderOptions
															.collapse_logo
													}
												/>
											);
										}
									})()}
								</div>
							);
						}
					})()}
					<Menu
						theme="dark"
						mode="inline"
						defaultSelectedKeys={
							this.props.location_path
								? [this.props.location_path.split('/')[2]]
								: []
						}
						defaultOpenKeys={
							this.props.location_path
								? [this.props.location_path.split('/')[2]]
								: []
						}
					>
						{(() => {
							if (this.props.menuOptions) {
								console.log(
									'data__',
									this.props.active_menu_key
								);
								return this.props.menuOptions.map(
									(data, index) => {
										if (
											data.submenu_items &&
											data.submenu_items.length
										) {
											return (
												<SubMenu
													className={
														this.props.location_path.includes(
															this.props
																.active_menu_key
														)
															? 'ant-menu-submenu-selected'
															: ''
													}
													key={data.key}
													onClick={() =>
														this.props.currentSubMenuData(
															data
														)
													}
													title={
														<span>
															{(() => {
																if (
																	data.icon_type
																) {
																	return (
																		<LegacyIcon
																			type={
																				data.icon_type
																			}
																		/>
																	);
																}
															})()}
															<span>
																{data.page_name}
															</span>
														</span>
													}
												>
													{(() => {
														if (
															data.submenu_items &&
															data.submenu_items
																.length
														) {
															return data.submenu_items.map(
																(
																	submenu_data
																) => {
																	console.log(
																		'submenu_data_',
																		submenu_data
																	);
																	return (
																		<Menu.Item
																			className={
																				this.props.location_path.includes(
																					this
																						.props
																						.active_submenu_key
																				) &&
																				submenu_data.key ==
																					this
																						.props
																						.active_submenu_key
																					? 'ant-menu-item-selected'
																					: ''
																			}
																			key={
																				submenu_data.key
																			}
																		>
																			<Link
																				to={
																					submenu_data.url
																				}
																			>
																				{
																					submenu_data.name
																				}
																			</Link>
																		</Menu.Item>
																	);
																}
															);
														}
													})()}
												</SubMenu>
											);
										} else {
											return (
												<Menu.Item
													className={
														data.key &&
														this.props
															.location_path &&
														this.props
															.active_menu_key
															? data.key ==
																	'dashboard' &&
															  (this.props
																	.active_menu_key ==
																	undefined ||
																	this.props
																		.active_menu_key ==
																		null ||
																	this.props
																		.active_menu_key ==
																		'')
																? 'ant-menu-item-selected'
																: this.props.location_path.includes(
																		this
																			.props
																			.active_menu_key
																  ) &&
																  data.key ==
																		this
																			.props
																			.active_menu_key
																? 'ant-menu-item-selected'
																: ''
															: this.props
																	.custom_menu &&
															  data.menu_value &&
															  this.props
																	.param_value
															? parseInt(
																	this.props
																		.active_menu_key
															  ) ===
															  parseInt(
																	this.props
																		.param_value
															  )
																? 'ant-menu-item-selected'
																: ''
															: ''
													}
													key={data.key}
													onClick={() =>
														this.props.currentMenuData(
															data
														)
													}
												>
													{(() => {
														if (data.url) {
															return (
																<Link
																	to={
																		data.url
																	}
																>
																	{(() => {
																		if (
																			data.icon_type
																		) {
																			return (
																				<LegacyIcon
																					type={
																						data.icon_type
																					}
																				/>
																			);
																		}
																	})()}
																	<span>
																		{
																			data.page_name
																		}
																	</span>
																</Link>
															);
														}
														if (
															this.props
																.onMenuClick &&
															data.menu_value
														) {
															return (
																<div
																	onClick={() =>
																		this.props.onMenuClick(
																			data.menu_value
																		)
																	}
																>
																	<span>
																		{
																			data.page_name
																		}
																	</span>
																</div>
															);
														}
													})()}
												</Menu.Item>
											);
										}
									}
								);
							}
						})()}
					</Menu>
				</Sider>
			</Layout>
		);
	}
}
