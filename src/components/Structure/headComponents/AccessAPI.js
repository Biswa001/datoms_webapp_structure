import { callFetch, temperatureCallFetch } from './ApiHandling';
import { notification } from 'antd';

async function getUserOptions(client_id, app_name) {
	try{
				let configData = {
						url : '/customers/'+client_id+'/applications/'+app_name+'/options',
						method : 'GET',
				}
				return await callFetch(configData);
		}catch(err){
				throw err;
	}	
}

async function setPreferences(user_id, client_id, application_id, preferences) {
	return new Promise((resolve, reject) => {
		fetch(process.env.REACT_APP_IOT_PLATFORM_USERS_API_BASEPATH + '/users/' + user_id + '/clients/' + client_id + '/applications/' + application_id + '/preferences', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include',
			body: JSON.stringify({
				"preferences": preferences
			})
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			resolve(json);
		}).catch((ex) => {
			// console.log('parsing failed', ex);
			openNotification('error', ex.message);
			reject('Unable to load data!');
		});
	});
}

function openNotification(type, msg){
		notification[type]({
				message: msg,
				// description: msg,
				placement: 'bottomLeft',
				className: 'alert-' + type,
		});
}

export { getUserOptions, setPreferences };