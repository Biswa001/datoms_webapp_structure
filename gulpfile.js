const gulp = require('gulp'),
    less = require('gulp-less'),
    LessAutoprefix = require('less-plugin-autoprefix'),
    autoprefix = new LessAutoprefix({ browsers: ['last 2 versions'] }),
    sourcemaps = require('gulp-sourcemaps');

gulp.task('less', () => {
    return gulp.src('./src/**/*.less')
        .pipe(sourcemaps.init())
        .pipe(less({
            plugins: [autoprefix],
            javascriptEnabled: true,
            modifyVars: { '@primary-color': '#f58740' },
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build'));
});
