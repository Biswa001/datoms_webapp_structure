# Datoms React Component Library

The repo contains various React Components used in the Datoms Frontend Web App.

## FAQs

### Why publish as plain javascript (after transpilation) & not as source code?

1. https://stackoverflow.com/questions/57761818/create-react-app-transpile-jsx-of-external-package-in-node-modules
2. https://github.com/facebook/create-react-app/issues/5103#issuecomment-425459196
