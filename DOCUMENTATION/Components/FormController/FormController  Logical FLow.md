## FORM CONTROLLER REACT COMPONENT

### LOGICAL FLOW

```mermaid
graph TD
  A1[FormController] -->|Static Json| A2[State Initialised]
  A2 --> A3[JSON Parsed]
  A3 --> A4[eval - onFinish onFinishFailed onValuesChange onFieldsChange]
  A4 --> A5[initialValues initialised] --> A6[Form Instance initialised] -->A
  A[Json2Jsx] --> B{Any Elements in Json?}
  B -->|TRUE| ER1
  B -->|FALSE| C
  ER1{compo == null}
  ER1 -->|TRUE| ERM1[Throw Error]
  ER1 -->|FALSE| C1{type of child == object}
  C1 -->|TRUE| A
  C1 -->|FALSE| C[Component Converter] --> P[Process Props - eval from seval array, onChange, onClick]
  P --> R(Render Generated Element)
```
