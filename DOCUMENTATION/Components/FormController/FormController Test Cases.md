## FORM CONTROLLER REACT COMPONENT

### TEST CASES

**JSON STRUCTURE:**

**The Following keys in the static json shall be used:**

1. compo
2. props
3. child
4. state

---

**Functional props, which are to be evaluated using eval**

-   `seval[]` : Contains Lists of prop names to be evalled.
    ```
    seval:['options','value']
    This would do the following:
    props.options = eval(props.options)
    props.value = eval(props.value)
    ```

```
NOTE:
props which are to be evaluated using eval can access
state variable as follows:
this.state should be written as context.state

Form Instance can be made use off using:
context.formRef instead of this.formRef
```

---

**The Following Props are accepted by FormController**

1. json
2. initialValues
3. onCallback

```
{
    "Form": {
        "compo": "Form",
        "state": {},
        "props": {
            "labelCol": {
                "span": "8"
            }
        },
        "child": {
            "Item1": {
                "compo": "Form.Item",
                "props": {
                    options:'()=>{}',
                    seval:['options']
                },
                "child": {
                    "Input": {
                        "compo": "Input"
                    }
                }
            }
        }
    }
}
```

---

**TEST CASES**

```
| Case | compo  | props | child  |                                                                                                                                                               output                                                                                                                                                                |
| :--: | :----: | :---: | :----: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|  1   | String | Json  | String |                                   The compo string will pass through a converter, which will give its corresponding Ant.Design component, if failed to convert, this string will be assumed to be html. The props will be passed to this component and the string in the child shall be displayed                                   |
|  2   | String | Json  |  Json  | The compo string will pass through a converter, which will give its corresponding Ant.Design component, if failed to convert, this string will be assumed to be html. The props will be passed to this component and the json in the child shall processed again as another component by passing to the same function by recursion. |
|  3   | String | Json  |  Null  |                            The compo string will pass through a converter, which will give its corresponding Ant.Design component, if failed to convert, this string will be assumed to be html. The props will be passed to this component. If no child element is to be displayed, null can be passed.                            |
|  4   | String | Null  | String |                                   The compo string will pass through a converter, which will give its corresponding Ant.Design component, if failed to convert, this string will be assumed to be html. No props will be passed to this component and the string in the child shall be displayed                                    |
|  5   | String | Null  |  Json  | The compo string will pass through a converter, which will give its corresponding Ant.Design component, if failed to convert, this string will be assumed to be html. No props will be passed to this component and the json in the child shall processed again as another component by passing to the same function by recursion.  |
|  6   | String | Null  |  Null  |                            The compo string will pass through a converter, which will give its corresponding Ant.Design component, if failed to convert, this string will be assumed to be html. No props will be passed to this component. If no child element is to be displayed, null can be passed.                             |
```

---

#### Default Values

Automatically initialised in the constructor if not passed explicitly.

|     Keys      |                    Default Value                     |
| :-----------: | :--------------------------------------------------: |
|     compo     |                         Null                         |
|     props     |                         Null                         |
|     child     |                         Null                         |
|     state     |                         Null                         |
|     json      | { Form: { compo: 'h2', child: 'No Json Provided' } } |
| initialValues |                         Null                         |
|  onCallback   |                         Null                         |

---

#### Cases Not Listed

-   Any case, which hasn't been listed above, shall throw error.

```$xslt
WARNING:
compo = NULL; // will throw error
```

---
