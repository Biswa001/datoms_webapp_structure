## TABLE_LIST REACT COMPONENT

### Logic

```mermaid
graph TD
    A[Index] -->|showLegend, breakPoint, subShowLegend, subBreakPoint, horizontalScroll, tableName| B[Constructor]
    B[Constructor] -->M{horizontalScroll==true}
    M -->|true| O{viewport width > breakPoint}
    O -->|true| N[ Render Table with Column Names and only if Table > BreakPoint, Horizontal Scroll will be Visible]
    O -->|false| P[Render Table With Horizontal scrolling enabled if width below the set breakpoint]
    M -->|false| C{viewport width > breakpoint  }
    C -->|true| D[Render Table with Column Names and No Horizontal Scroll]
    C -->|false| F{showLegend==true}
    F -->|true| G[List with All Column Names Visible, Except for Columns with subShowLegend = false. Column Name and Data shall be inline unless subBreakPoint is Specified and subBreakPoint > ViewPort width]
    F -->|false| H[List Without Column Names, Except for Columns with subShowLegend = true. Column Name and Data shall be inline unless subBreakPoint is Specified and subBreakPoint > ViewPort width]
```
